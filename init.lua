-- Mod SkinDB

-- This mod adds a functions to download skins from skin-db and fetch informations.

local MODPATH = minetest.get_modpath("skindb");
--dofile(MODPATH.."/ee5_base64.lua");--very fast, but requires luajit 2.x or Lua 5.3

csv = io.open(MODPATH.."/debug.csv","w")
csv:write(string.format("%s;%s\n","ID","Prozessing time in ms"))
function unpack (t, i)--builtin unpack function seems to be broken.
     i = i or 1
      if t[i] ~= nil then
        return t[i], unpack(t, i + 1)
      end
    end

function get_str(t)

	local str = ""--table.concat(t,"");
	for i,c in ipairs(t) do
		--if( c ~= nil ) then
			str = str..string.char(c)
		--end
	end
	return str;
end

dofile(MODPATH.."/base64.lua");

local http = assert(minetest.request_http_api(), "ERROR: Could not get http api!");


local url = "http://minetest.fensta.bplaced.net/api/get.json.php?getlist&per_page=30&page=%d";
local last_page = 1;
local tries = 0;
local filename = MODPATH.."/textures/%s_by_%s.png";
minetest.mkdir(MODPATH.."/textures/");
local function callback(resp)
	--Todo: proper error handling
	if(resp.succeeded == true) then
		tries = 0;
		local page = minetest.parse_json(resp.data)
		
		for i,skin in ipairs(page.skins) do
			--minetest.debug(dump(skin));
			local t1 = os.clock()
			SkinDB.registered_skins[skin.id] = Skin.new(skin);
			csv:write(string.format("%d;%.2f\n",skin.id,(os.clock() - t1) * 1000 ))
		end
		if page.page < page.pages then
			http.fetch({url=url:format(page.page +1)}, callback)
			last_page = page.page +1;
		end
		
	elseif resp.timeout == true then
		if(tries < 5) then
			minetest.log("connection timed out");
			--try again
			SkinDB.fetch(last_page);
			tries = tries +1;
		else
			error("Could not fetch page"..last_page.."from skin DB after 5 tries")
		end
	end
end

SkinDB = {
	fetch = function(page)
	http.fetch({url=url:format(page)}, callback)
	last_page = page;
	end,
	registered_skins = {};
}



SkinDB.fetch(1);

Skin = {}
Skin.__index = Skin

setmetatable(Skin, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Skin.new(init)
	local self = setmetatable({}, Skin)
	if type(init.name) == "number" then init.name = string.format("%d",init.name) end
	if type(init.author) == "number" then init.author = string.format("%d",init.author)end
	self.name = init.name or "unnamed"
	self.id = init.id or 0;
	self.author = init.author or "unknown"
	if init.img then
		local img = base64.decode(init.img)

		local file = io.open(Skin.getFileName(self), "wb")
		if file then 
			file:write(img);
			file:close();
		else
			--error
		end
	else
		self.img= ""
	end

	return self
end

function Skin:getFileName()
	return filename:format(self.name:gsub("[^%w]",""),self.author:gsub("[^%w]",""));

end

